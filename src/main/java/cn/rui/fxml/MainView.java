package cn.rui.fxml;

import de.felixroske.jfxsupport.AbstractFxmlView;
import de.felixroske.jfxsupport.FXMLView;

/**
 * @author chenwenxi
 */
@FXMLView(title = "百度地图采集(锐哥专用)",value = "/cn/rui/fxml/Main.fxml")
public class MainView extends AbstractFxmlView {
}
