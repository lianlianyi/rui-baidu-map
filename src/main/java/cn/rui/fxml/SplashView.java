package cn.rui.fxml;

import cn.hutool.core.io.resource.ResourceUtil;
import de.felixroske.jfxsupport.SplashScreen;
import javafx.scene.Parent;
import javafx.scene.control.ProgressBar;
import javafx.scene.image.ImageView;
import javafx.scene.layout.VBox;

/**
 * @author chenwenxi
 */
public class SplashView extends SplashScreen {

	@Override
	public String getImagePath() {
		// 其实这里return什么都没有影响，关键是getParent方法
		return "splashScreen.jpg";
	}

	// 返回闪屏的界面
	@Override
	public Parent getParent() {
		final ImageView imageView = new ImageView(ResourceUtil.getResource("images/splashScreen.jpg").toExternalForm());
		final ProgressBar splashProgressBar = new ProgressBar();
		splashProgressBar.setPrefWidth(imageView.getImage().getWidth());

		final VBox vbox = new VBox();
		vbox.getChildren().addAll(imageView, splashProgressBar);

		return vbox;
	}

}
