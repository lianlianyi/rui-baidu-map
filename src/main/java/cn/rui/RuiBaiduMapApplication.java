package cn.rui;

import cn.rui.fxml.MainView;
import cn.rui.fxml.SplashView;
import de.felixroske.jfxsupport.AbstractJavaFxApplicationSupport;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * @author chenwenxi
 */
@ComponentScan(basePackages={"cn.rui","cn.hutool.extra.spring"})
@EnableScheduling
@RequiredArgsConstructor
@SpringBootApplication
public class RuiBaiduMapApplication extends AbstractJavaFxApplicationSupport {

    public static void main(String[] args) {
        launch(RuiBaiduMapApplication.class, MainView.class, new SplashView(),args);
    }

}
