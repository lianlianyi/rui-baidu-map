package cn.rui.baidu.map.res;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@NoArgsConstructor
@Data
public class Search {

    /**
     * status : 0
     * message : ok
     * total : 98
     * result_type : poi_type
     * results : [{"name":"海澜之家(北京西城区黄寺大街店)","location":{"lat":39.970754,"lng":116.391887},"address":"北京市西城区黄寺大街23号-2号楼1层-2-2号","province":"北京市","city":"北京市","area":"西城区","street_id":"6b014744364973ef83288aba","telephone":"(010)82237927","detail":1,"uid":"6b014744364973ef83288aba"},{"name":"酷吧男装","location":{"lat":40.04884,"lng":116.676679},"address":"北京市顺义区李家桥北中心街与通顺路交叉路口东侧(智慧幼儿园东北侧约50米)","province":"北京市","city":"北京市","area":"顺义区","street_id":"cfba7b8e272094c2c72ba167","telephone":"13718889120","detail":1,"uid":"cfba7b8e272094c2c72ba167"},{"name":"MADNESS(三里屯太古里店)","location":{"lat":39.940788,"lng":116.461233},"address":"北京市朝阳区三里屯路19号三里屯太古里F2","province":"北京市","city":"北京市","area":"朝阳区","street_id":"9dc9a6d014a1721ac8213d73","telephone":"(010)84404251","detail":1,"uid":"9dc9a6d014a1721ac8213d73"},{"name":"GXG(龙德广场店)","location":{"lat":40.0664,"lng":116.4221},"address":"北京市昌平区立汤路186号龙德广场F1","province":"北京市","city":"北京市","area":"昌平区","street_id":"4585557f96a77f1ac51b759c","telephone":"(010)83017076","detail":1,"uid":"4585557f96a77f1ac51b759c"},{"name":"劲霸男装","location":{"lat":39.914245,"lng":116.583105},"address":"北京市朝阳区双桥路2号院2西80米","province":"北京市","city":"北京市","area":"朝阳区","detail":1,"uid":"2cb737bf32f4b35b81a12edc"},{"name":"PULL&BEAR(远洋未来广场购物中心店)","location":{"lat":39.996694,"lng":116.439264},"address":"北京市朝阳区东北四环73号未来广场购物中心F2","province":"北京市","city":"北京市","area":"朝阳区","street_id":"a50b174ec065dbeb2cb908d3","telephone":"(010)84445626","detail":1,"uid":"a50b174ec065dbeb2cb908d3"},{"name":"迪格男装","location":{"lat":39.945777,"lng":116.107683},"address":"北京市门头沟区新桥大街国泰百货三层电梯旁边","province":"北京市","city":"北京市","area":"门头沟区","detail":1,"uid":"f6de2ee94ec4c3dd8c1a8982"},{"name":"欧尚精品男装","location":{"lat":40.227049,"lng":116.260115},"address":"北京市昌平区府学路16号金五星市场3层","province":"北京市","city":"北京市","area":"昌平区","street_id":"4b87a90ba6df7cb45a3ac89c","telephone":"18800009471","detail":1,"uid":"4b87a90ba6df7cb45a3ac89c"},{"name":"海澜之家(隆华奥特莱斯店)","location":{"lat":40.134678,"lng":116.648408},"address":"北京市顺义区贯通路18号隆华奥特莱斯购物广场F1","province":"北京市","city":"北京市","area":"顺义区","street_id":"339a0a618a6134980289726d","telephone":"(010)60416298","detail":1,"uid":"339a0a618a6134980289726d"},{"name":"优E酷克男装","location":{"lat":40.10486,"lng":116.380781},"address":"北七家平西王府村","province":"北京市","city":"北京市","area":"昌平区","street_id":"7c604fcd88e32b9fa4c756c7","detail":1,"uid":"7c604fcd88e32b9fa4c756c7"},{"name":"Massimo Dutti(远洋未来广场购物中心店)","location":{"lat":39.996394,"lng":116.438431},"address":"北京市朝阳区东北四环73号未来广场购物中心F1","province":"北京市","city":"北京市","area":"朝阳区","street_id":"632ceb563b20b0d61d58001a","telephone":"(010)84445612","detail":1,"uid":"632ceb563b20b0d61d58001a"},{"name":"美良(工厂店)","location":{"lat":39.738149,"lng":116.431797},"address":"金服大街15号","province":"北京市","city":"北京市","area":"大兴区","street_id":"53ea57aba690b69d43ab9a81","telephone":"(010)69273311","detail":1,"uid":"53ea57aba690b69d43ab9a81"},{"name":"男装","location":{"lat":39.978846,"lng":116.401395},"address":"北京市朝阳区安贞西里二区21号楼底商","province":"北京市","city":"北京市","area":"朝阳区","detail":1,"uid":"51249e5fd6ecde6b2bd7ab65"},{"name":"大码男装","location":{"lat":39.936634,"lng":116.423839},"address":"北京市东城区东四北大街310号附近","province":"北京市","city":"北京市","area":"东城区","street_id":"786d7b06f7ba75ee78f796ed","telephone":"13031058810,13621121795","detail":1,"uid":"786d7b06f7ba75ee78f796ed"},{"name":"Abercrombie&Fitch(三里屯太古里南区S6店)","location":{"lat":39.940821,"lng":116.461685},"address":"北京市朝阳区三里屯路19号三里屯太古里F2","province":"北京市","city":"北京市","area":"朝阳区","street_id":"1aa53e04b0d1df9814c8f63c","telephone":"(010)84511892","detail":1,"uid":"1aa53e04b0d1df9814c8f63c"},{"name":"海澜之家(北京西城区华威大厦店)","location":{"lat":39.917556,"lng":116.380873},"address":"北京市西城区西单北大街130号华威大厦三层","province":"北京市","city":"北京市","area":"西城区","telephone":"(010)66155640","detail":1,"uid":"1253f3d8d63723a5260bd072"},{"name":"恒源祥(百荣世贸商城店)","location":{"lat":39.867274,"lng":116.405208},"address":"北京市东城区永定门外大街101-1号百荣世贸商城Ｃ座4层","province":"北京市","city":"北京市","area":"东城区","street_id":"d44d67e0fd02a7b1b020a850","telephone":"15801313159","detail":1,"uid":"d44d67e0fd02a7b1b020a850"},{"name":"圣罗兰(北京SKP男装店)","location":{"lat":39.915684,"lng":116.485643},"address":"北京市朝阳区建国路87号北京SKP二层M2005-M2006号商铺","province":"北京市","city":"北京市","area":"朝阳区","street_id":"689155a1fd9ee0d67bdbd2ad","telephone":"(010)85888928","detail":1,"uid":"689155a1fd9ee0d67bdbd2ad"},{"name":"随意时尚男装","location":{"lat":39.871253,"lng":116.483797},"address":"北京市朝阳区西大望南路与弘燕南一路交叉路口往北约50米(周庄嘉园)","province":"北京市","city":"北京市","area":"朝阳区","street_id":"","telephone":"13601015733","detail":1,"uid":"badb0ffe9f75b646b21e366a"},{"name":"鼎盛男装","location":{"lat":40.118675,"lng":116.222417},"address":"北京市海淀区甲屯路与上庄中路交叉路口往东约200米(小天骄幼儿园东北侧)","province":"北京市","city":"北京市","area":"海淀区","detail":1,"uid":"0462fe37c62efcbbdb131812"}]
     */

    private Integer status;
    private String message;
    private Integer total;
    private String result_type;
    private List<ResultsBean> results;

    @NoArgsConstructor
    @Data
    public static class ResultsBean {
        /**
         * name : 海澜之家(北京西城区黄寺大街店)
         * location : {"lat":39.970754,"lng":116.391887}
         * address : 北京市西城区黄寺大街23号-2号楼1层-2-2号
         * province : 北京市
         * city : 北京市
         * area : 西城区
         * street_id : 6b014744364973ef83288aba
         * telephone : (010)82237927
         * detail : 1
         * uid : 6b014744364973ef83288aba
         */

        private String name;
        private LocationBean location;
        private String address;
        private String province;
        private String city;
        private String area;
        private String street_id;
        private String telephone;
        private Integer detail;
        private String uid;

        @NoArgsConstructor
        @Data
        public static class LocationBean {
            /**
             * lat : 39.970754
             * lng : 116.391887
             */

            private Double lat;
            private Double lng;
        }
    }
}
