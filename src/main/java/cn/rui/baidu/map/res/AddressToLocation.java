package cn.rui.baidu.map.res;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 地理编码
 */
@NoArgsConstructor
@Data
public class AddressToLocation {

    /**
     * status : 0
     * result : {"location":{"lng":116.68852864054833,"lat":23.35909171772515},"precise":0,"confidence":20,"comprehension":100,"level":"城市"}
     */
    private Integer status;
    private ResultBean result;

    @NoArgsConstructor
    @Data
    public static class ResultBean {
        /**
         * location : {"lng":116.68852864054833,"lat":23.35909171772515}
         * precise : 0
         * confidence : 20
         * comprehension : 100
         * level : 城市
         */
        private LocationBean location;
        private Integer precise;
        private Integer confidence;
        private Integer comprehension;
        private String level;

        @NoArgsConstructor
        @Data
        public static class LocationBean {
            /**
             * lng : 116.68852864054833
             * lat : 23.35909171772515
             */
            private Double lng;
            private Double lat;
        }
    }
}
