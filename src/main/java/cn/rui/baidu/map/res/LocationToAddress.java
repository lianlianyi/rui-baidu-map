package cn.rui.baidu.map.res;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * 逆地理编码
 */
@NoArgsConstructor
@Data
public class LocationToAddress {

    /**
     * status : 0
     * result : {"location":{"lng":116.68852864054831,"lat":23.35909173251204},"formatted_address":"广东省汕头市金平区跃进路30号","business":"中山公园,利安路,南海路","addressComponent":{"country":"中国","country_code":0,"country_code_iso":"CHN","country_code_iso2":"CN","province":"广东省","city":"汕头市","city_level":2,"district":"金平区","town":"","town_code":"","adcode":"440511","street":"跃进路","street_number":"30号","direction":"附近","distance":"14"},"pois":[],"roads":[],"poiRegions":[],"sematic_description":"","cityCode":303}
     */

    private Integer status;
    private ResultBean result;

    @NoArgsConstructor
    @Data
    public static class ResultBean {
        /**
         * location : {"lng":116.68852864054831,"lat":23.35909173251204}
         * formatted_address : 广东省汕头市金平区跃进路30号
         * business : 中山公园,利安路,南海路
         * addressComponent : {"country":"中国","country_code":0,"country_code_iso":"CHN","country_code_iso2":"CN","province":"广东省","city":"汕头市","city_level":2,"district":"金平区","town":"","town_code":"","adcode":"440511","street":"跃进路","street_number":"30号","direction":"附近","distance":"14"}
         * pois : []
         * roads : []
         * poiRegions : []
         * sematic_description :
         * cityCode : 303
         */

        private LocationBean location;
        private String formatted_address;
        private String business;
        private AddressComponentBean addressComponent;
        private List<?> pois;
        private List<?> roads;
        private List<?> poiRegions;
        private String sematic_description;
        private Integer cityCode;

        @NoArgsConstructor
        @Data
        public static class LocationBean {
            /**
             * lng : 116.68852864054831
             * lat : 23.35909173251204
             */

            private Double lng;
            private Double lat;
        }

        @NoArgsConstructor
        @Data
        public static class AddressComponentBean {
            /**
             * country : 中国
             * country_code : 0
             * country_code_iso : CHN
             * country_code_iso2 : CN
             * province : 广东省
             * city : 汕头市
             * city_level : 2
             * district : 金平区
             * town :
             * town_code :
             * adcode : 440511
             * street : 跃进路
             * street_number : 30号
             * direction : 附近
             * distance : 14
             */

            private String country;
            private Integer country_code;
            private String country_code_iso;
            private String country_code_iso2;
            private String province;
            private String city;
            private Integer city_level;
            private String district;
            private String town;
            private String town_code;
            private String adcode;
            private String street;
            private String street_number;
            private String direction;
            private String distance;
        }
    }
}
